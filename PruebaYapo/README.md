# Prueba YAPO
_Prueba automatizada _

## Autor ✒️
* **Carla Chelme**   - *Ingeniera QA Automation* - (carla.chelmerivera@gmail.com)


## Precondiciones

**Tener Instalado: **
* Eclipse </br>
* Cucumber </br>
* jdk 

**Variables de entorno JAVA HOME con su path correspondiente al jdk instalado**

## Estructura del proyecto

* **controller** 	: funciones asociadas al negocio
* **definition**	: métodos generados por cucumber
* **driver**	: configuraciones propias del driver
* **entities**		: clase producto
* **flow**        	: levanta el driver
* **flow/unitest**	: ejecución de escenarios
* **pages**			: declaración de webelements	

## Para Ejecutar

Click derecho sobre archivo **UnitTest** del directorio **flow.unitest** y seleccionar **run as -> JUnit Test**

Nota: en caso de presentar problemas en la ejecución bajar a junit4

## Descripción del ejercicio
1.Automatización: Automatizar consulta de productos en un retail dado un listado en un
archivo CSV
Imagina que perteneces a un equipo de IT de alguna empresa de retail y quieres verificar la
funcionalidad de búsqueda/detalle de un producto (considerando una visión de caja negra).
Para verificar esto, te solicitamos que escribas un script que:


1.0. Definas qué sitio web de retail vas a utilizar para realizar tus tareas de automatización.
Puede ser un sitio web nacional o internacional.


1.1. Consideres como punto de entrada un archivo CSV que contenga un listado de nombres
de producto (Ej: zelda breath of the wild, mass effect, super mario maker )


1.2. Por cada nombre de producto, deberás ir al sitio web del retail que seleccionaste y realizar
una búsqueda con ese nombre de producto. Si la búsqueda te arrojó resultados, debería
considerar únicamente el primer resultado


1.3. Toma ese primer resultado e ingresa al sitio web en donde se encuentra el detalle del
producto. Ahí deberás obtener la siguiente información desde ese sitio web: código de producto
(sku o similar), disponibilidad (ya sea en tienda o en el sitio web). Recomiendo que vayas
almacenando esta información en una variable global.


1.4. Toma la variable global que contiene cada producto junto con la información que
recolectaste de la web y genera un archivo CSV de salida que contenga las siguientes
columnas
- Nombre de Producto
- Código de producto (SKU o similar)
- Si tiene disponibilidad en tienda (true or false)
- Si tiene disponibilidad en el sitio web (true or false)
Entregable:
- Un script (en la herramienta que tu consideres mejor) que permita la ejecución de la tarea de
automatización. Ideal que lo puedas subir en alguna plataforma gratuita tipo: GitLab o Github
- Un CSV de entrada de ejemplo
- Instrucciones para consumir el script
