package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProductoPage {

    @FindBy(className = "ais-SearchBox-input")
    private WebElement txtBuscaProducto;

    @FindBy(className = "ais-input-search-button")
    private WebElement btnBuscar;
    
    @FindBy(className = "ais-Hits-item")
    private WebElement itenmProd;

    @FindBy(xpath = "//*[@id=\"id_ficha_producto\"]/div[3]/div[2]/div[2]/p[1]/span")
    private WebElement lblSku;
    

    @FindBy(xpath = "//*[@id=\"id_ficha_producto\"]/div[3]/div[6]/div[1]/div[2]/p")
    private WebElement lblDomicilio;
    
    @FindBy(xpath = "//*[@id=\"id_ficha_producto\"]/div[3]/div[6]/div[2]/div[2]/p")
    private WebElement  lblTienda;

	public WebElement getTxtBuscaProducto() {
		return txtBuscaProducto;
	}

	public WebElement getBtnBuscar() {
		return btnBuscar;
	}

	public WebElement getItenmProd() {
		return itenmProd;
	}

	public WebElement getLblSku() {
		return lblSku;
	}

	public WebElement getLblDomicilio() {
		return lblDomicilio;
	}

	public WebElement getLblTienda() {
		return lblTienda;
	}
    
    
	
}
