package controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import entities.Producto;
import flow.BaseConfig;

public class PruebaController {


    private static final Logger LOGGER = LoggerFactory.getLogger("");
    
	  /**
 * <b>Nombre:</b>recorroCsv</br>
 * </br>
 * <b>Description:</b> Recorro csv de archivo Documentos
 *
 * @param {@link List<Producto> listProd} 
 **/
	public static void recorroCsv() {
		try {
			String archCSV = "Documentos/listaProducto.csv";
			CSVReader csvReader = new CSVReader(new FileReader(archCSV));
			String[] fila = null;
			
			List<Producto> listProd =new ArrayList<Producto>();
			while((fila = csvReader.readNext()) != null) {
			    listProd.add(buscoProducto(fila[0]));
			}

			csvReader.close();
			generoCsv(listProd);
		} catch (Exception e) {
			LOGGER.error("Error al recorrer csv. " + e.getMessage());
		}
	
		
	}
	
	  /**
   * <b>Nombre:</b>generoCsv</br>
   * </br>
   * <b>Description:</b> Busca datos del producto sku y disponibilidad de despacho a domicilio o retiro en tienda
   *
   * @param {@link List<Producto> listProd} 
   **/
	public static void generoCsv(List<Producto> listProd) {
		try {
		
			String currentPath = Paths.get("").toAbsolutePath().normalize().toString();
            String downloadFolder = "/Documentos/ArchivoRespuesta";
            String downloadPath = currentPath + downloadFolder;
            File newFolder = new File(downloadPath);
            newFolder.mkdir();

            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-M-dd_HH-mm-ss");
            LocalDateTime now = LocalDateTime.now();
            String fileName = "listaProdDetalle_" + dtf.format(now) + ".csv";

            File statText = new File(downloadPath + "/" + fileName);
            FileOutputStream is = new FileOutputStream(statText);
            OutputStreamWriter osw = new OutputStreamWriter(is);

			String archCSV = downloadPath + "/" + fileName ;
			CSVWriter writer = new CSVWriter(new FileWriter(archCSV));

			 String [] enunciado = {"Nombre Producto","SKU", "Disponible domicilio", "Disponible Tienda"};
			 writer.writeNext(enunciado);
			 
			for (Producto auxProducto : listProd) {
				 String [] prods = {auxProducto.getNombreProducto(),auxProducto.getSku(), String.valueOf(auxProducto.isDomicilio()), String.valueOf(auxProducto.isTienda())};
				writer.writeNext(prods);	
			}
			writer.close();
			
		} catch (Exception e) {
			LOGGER.error("Error al recorrer csv. " + e.getMessage());
		}
	
		
	}
	
	  /**
     * <b>Nombre:</b>buscoProducto</br>
     * </br>
     * <b>Description:</b> Busca datos del producto sku y disponibilidad de despacho a domicilio o retiro en tienda
     *
     * @param {@link String} Forma
     * @return {@link Producto} 
     **/
	public static Producto buscoProducto(String prod) {
		Producto auxProducto = new Producto();
		try {
			BaseConfig.prodPage.getTxtBuscaProducto().sendKeys(prod);
			BaseConfig.prodPage.getBtnBuscar().click();
			BaseConfig.prodPage.getItenmProd().click();
			auxProducto.setNombreProducto(prod);
			auxProducto.setSku(BaseConfig.prodPage.getLblSku().getText());
			auxProducto.setDomicilio(estadisponible("domicilio"));
			auxProducto.setTienda(estadisponible("tienda"));
			
		} catch (Exception e) {
			LOGGER.error("Error al buscar producto. " + e.getMessage());
		}
		
		return auxProducto;
	}
	
	  /**
     * <b>Nombre:</b>estadisponible</br>
     * </br>
     * <b>Description:</b> Verifica la existencia de un elemento
     *
     * @param {@link String} Forma
     * @return {@link Boolean} Retorna <b>True</b> si el producto esta disponible <b>False</b>
     **/
	public static boolean estadisponible(String forma) {
		boolean resp = false;
		try {
			switch (forma) {
			case "domicilio":
				if(BaseConfig.prodPage.getLblDomicilio().getText().equals("Disponible")) {
					resp =true;
				}
				break;
			case "tienda":
				if(BaseConfig.prodPage.getLblTienda().getText().equals("Disponible")) {
					resp =true;
				}
				break;
			}
			
			
		} catch (Exception e) {
			LOGGER.error("Error al verificar si esta disponible en " + forma + ". " +e.getMessage());
		}
		return resp;
	}
	
	  /**
     * <b>Nombre:</b>estaPresente</br>
     * </br>
     * <b>Description:</b> Verifica la existencia de un elemento
     *
     * @param {@link WebElement} Objeto de tipo WebElement a buscar
     * @return {@link Boolean} Retorna <b>True</b> si el elemento es encontrado, de
     * lo contrario retorna <b>False</b>
     **/
    public static boolean estaPresente(WebElement webElement) {
        boolean resp = true;
        BaseConfig.driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        try {
            WebDriverWait wait = new WebDriverWait(BaseConfig.driver, 15);
            wait.until(ExpectedConditions.visibilityOf(webElement));
        } catch (Exception e) {
            resp = false;
        } finally {
            BaseConfig.driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        }
        return resp;
    }
	
	
}
