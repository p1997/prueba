package flow.unitest;

import com.github.mkolisnyk.cucumber.runner.ExtendedCucumber;
import cucumber.api.CucumberOptions;
import flow.BaseConfig;

import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;
import org.junit.AfterClass;
import org.junit.runner.RunWith;


@RunWith(ExtendedCucumber.class)
@ExtendedCucumberOptions(
        jsonReport = "Reports/cucumber.json",
        //jsonUsageReport = "Reporte/cucumber-usage.json",
        screenShotLocation = "Screenshots/",
        screenShotSize = "550px",
        detailedReport = true,
        //detailedAggregatedReport = true
        //overviewReport = true,
        //overviewChartsReport = true,
        //featureOverviewChart = true,
        //usageReport = true,
        //coverageReport = true,
        toPDF = true,
        pdfPageSize = "letter",
        outputFolder = "Reports/")

	@CucumberOptions
	(plugin = {"json:Reports/cucumber.json"},
	features = {"src/test/resources/features"},
	glue = {"definition"},

		       tags = {
	   "@PruebaProducto"
	})


public class UnitTest extends BaseConfig {
    @AfterClass
    public static void setUpFinal() throws Exception {
        //String rutaXML = System.getProperty("user.dir")+"/src/test/resources/config/extent-config.xml";
        //Reporter.loadXMLConfig(rutaXML);
    }
}
