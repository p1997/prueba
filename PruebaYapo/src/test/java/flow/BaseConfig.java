package flow;

import driver.*;
import pages.ProductoPage;

import org.apache.log4j.BasicConfigurator;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;


public class BaseConfig {

    public static WebDriver driver;
    public static ProductoPage prodPage;

    @BeforeClass()
    public static void beforeMethodSetup() throws Exception {
        BasicConfigurator.configure();
        DriverContext.setUp("https://www.pcfactory.cl/");
        DriverContext.setDriverTimeout(5);
        DriverContext.setScriptTimeout(5);
        driver = DriverContext.getDriver();
        PageFactory.initElements(driver, BaseConfig.class);
        prodPage = PageFactory.initElements(driver, ProductoPage.class);
    }

    @AfterClass()
    public static void afterMethodSetup() {
        DriverContext.deleteAllCookies();
        DriverContext.quitDriver();
    }
}
