package driver;


import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class DriverManager {

    private static WebDriver driver;
    private File root = new File("WebDriver/Chrome/Windows"); //Inicializada para windows
    private String extensionDriver = "";

    public DriverManager() {
    }

    protected void resolveDriver(String ambURL) {
        String os = System.getProperty("os.name").toLowerCase();
        System.out.println("\nSistema operativo ->" + System.getProperty("os.name").toLowerCase() + " " + System.getProperty("os.version").toLowerCase() + "\n");
   
                if (os.contains("windows")) {
                    this.extensionDriver = ".exe";
                    this.root = new File("WebDriver/Chrome/Windows");
                    this.root = new File(this.root, "chromedriver" + this.extensionDriver);
                }
                if (os.contains("mac")) {
                    this.root = new File("WebDriver/Chrome/MacOS");
                    this.root = new File(this.root, "chromedriver");
                }
                if (os.contains("linux")) {
                    this.root = new File("WebDriver/Chrome/Linux/64");
                    this.root = new File(this.root, "chromedriver");
                }
                System.setProperty("webdriver.chrome.driver", this.root.getAbsolutePath());
                ChromeOptions chromeOptions = new ChromeOptions();
                HashMap<String, Object> chromeLocalStatePrefs = new HashMap<String, Object>();
                List<String> experimentalFlags = new ArrayList<String>();
                experimentalFlags.add("same-site-by-default-cookies@2");
                chromeLocalStatePrefs.put("browser.enabled_labs_experiments", experimentalFlags);
                chromeOptions.setExperimentalOption("localState", chromeLocalStatePrefs);
                chromeOptions.addArguments("--ignore-certificate-errors");
                chromeOptions.addArguments("--disable-extensions");
                chromeOptions.addArguments("--disable-dev-shm-usage");
                chromeOptions.addArguments("--disable-gpu");
                chromeOptions.addArguments("--no-sandbox");
                if (os.contains("linux")) {
                    chromeOptions.addArguments("--headless");
                }
                driver = new ChromeDriver(chromeOptions);
                driver.manage().window().maximize();
             
        driver.get(ambURL);

    }

    protected WebDriver getDriver() {
        return driver;
    }

    protected void setDriver(WebDriver webDriver) {
        driver = webDriver;
    }

    protected Dimension getScreenSize() {
        return driver.manage().window().getSize();
    }
}
