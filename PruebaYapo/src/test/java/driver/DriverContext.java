package driver;


import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class DriverContext {
    private static DriverManager driverManager = new DriverManager();



    public DriverContext() {
    }


    public static void setUp(String ambURL) {
        driverManager.resolveDriver(ambURL);
    }

    public static WebDriver getDriver() {
        return driverManager.getDriver();
    }

    public static void setDriverTimeout(Integer tiempo) {
        driverManager.getDriver().manage().timeouts().implicitlyWait((long) tiempo, TimeUnit.SECONDS);
    }

    public static void quitDriver() {
        driverManager.getDriver().quit();
    }

    public static Dimension getSreenSize() {
        return driverManager.getScreenSize();
    }

    public static void deleteAllCookies() {
        driverManager.getDriver().manage().deleteAllCookies();
    }

    public static void setScriptTimeout(int time) {
        driverManager.getDriver().manage().timeouts().setScriptTimeout((long) time, TimeUnit.SECONDS);
    }
}

