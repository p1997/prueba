package entities;

public class Producto {


public String nombreProducto;
public String sku;
public boolean tienda;
public boolean domicilio;


public String getNombreProducto() {
	return nombreProducto;
}
public void setNombreProducto(String nombreProducto) {
	this.nombreProducto = nombreProducto;
}
public String getSku() {
	return sku;
}
public void setSku(String sku) {
	this.sku = sku;
}
public boolean isTienda() {
	return tienda;
}
public void setTienda(boolean tienda) {
	this.tienda = tienda;
}
public boolean isDomicilio() {
	return domicilio;
}
public void setDomicilio(boolean domicilio) {
	this.domicilio = domicilio;
}
	
	
}
