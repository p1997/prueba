package definition;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.junit.Assert.assertTrue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import controller.PruebaController;

import flow.BaseConfig;

public class BaseDefinition extends BaseConfig {
	public static Scenario Iscenario;
	private static final Logger LOGGER = LoggerFactory.getLogger("");// creacion de variable logger log para escribir


	@Before // tag que indica que este metodo se ejecutara antes de cada scenario
	public void iniciaScenario(Scenario scenario) {// metodo que escribira el nombre del scenario
		Iscenario = scenario;
		LOGGER.info("Inicio de escenario : " + scenario.getName());
	}

	@After // tag que indica que se ejcutara despues de cada metodo
	public void insertaImage(Scenario scenario) throws Exception {
		LOGGER.info("Estado del escenario: " + scenario.getStatus());
	}

	@When("^verifico que este habilitado el input de busqueda en pc factory$")
	public void verifico_que_este_habilitado_el_input_de_busqueda_en_pc_factory() throws Throwable {
		assertTrue("No se encuentr input buscar ",
				PruebaController.estaPresente(BaseConfig.prodPage.getTxtBuscaProducto()));
	}

	@Then("^busco productos listados en un archivo csv y genero un archivo csv de salida con la info recuperada$")
	public void busco_productos_listados_en_un_archivo_csv_y_genero_un_archivo_csv_de_salida_con_la_info_recuperada()
			throws Throwable {
		PruebaController.recorroCsv();
	}
}
